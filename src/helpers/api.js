const API_URL = 'https://blabladev.com'

/**
 * @name checkSession
 * @desc check if user is logged in
 */
function checkSession() {
	return fetch(`${API_URL}/user/session`, {
			method: 'GET',
			credentials: 'include'
		})
		.then(response => response.json())
}

/**
 * @name logOut
 * @desc log user out, delete cookie on server
 */
function logOut() {
	return fetch('https://blabladev.com/user/logout', {
		method: 'POST',
		credentials: 'include'
	})
}

/**
 * @name setUserPassword
 * @desc change user password
 * @param fd - form data object
 */
function setUserPassword(fd) {
	return fetch(`${API_URL}/user/set_user_password`, {
			method: 'POST',
			credentials: 'include',
			body: fd
		})
		.then(response => response.json())
}

/**
 * @name setUserProfile
 * @desc edit user profile
 * @param fd - form data object
 */
function setUserProfile(fd) {
	return fetch(`${API_URL}/user/set_user_profile`, {
			method: 'POST',
			credentials: 'include',
			body: fd
		})
		.then(response => response.json())
}

/**
 * @name uploadAvatar
 * @desc upload user avatar
 * @param fd - form data
 */
function uploadAvatar(fd) {
	return fetch('https://blabladev.com/user/upload_avatar', {
			method: 'POST',
			body: fd,
			credentials: 'include'
		})
		.then(response => response.json())
}

/**
 * @name getProjects
 * @desc returns all projects by bbDev
 */
function getProjects() {
	return fetch(`${API_URL}/project/list`, {
			method: 'GET',
			credentials: 'include',
		})
		.then(response => response.json())
		// .then(response => resolve(response))
		.catch(err => console.error(err));
}

/**
 * @name getDevProjects
 * @desc get developer projects
 */
function getDevProjects(projects, user) {
	return fetch(`${API_URL}/devproject/list/${user.user_id}`, {
			method: 'GET',
			credentials: 'include',
		})
		.then(response => response.json())
		.then(response => {
			console.log(response);
			for (let i = response.length - 1; i >= 0; i--) {
				const project = projects.find(pr => pr.id === response[i].project_id);

				response[i].avatar = project.avatar;
				response[i].company = project.company;
				response[i].title = project.title;
				response[i].level = project.level;
				response[i].languages = project.languages;

				user.projects.push(response[i]);
			}
		})
		.catch(err => console.error(err));
}


/**
 * @name uploadCV
 * @desc upload user resume
 * @param fd - form data obj
 */
function uploadCV(fd) {
	return fetch('https://blabladev.com/user/upload_cv', {
			method: 'POST',
			credentials: 'include',
			body: fd
		})
		.then(response => response.json())
}

/**
 * @name deleteFile
 * @desc delete file in project
 * @param fd - form data
 */
function deleteFile(fd) {
	return fetch(`${API_URL}/project/delete_file`, {
			method: 'POST',
			credentials: 'include',
			body: fd
		})
		.then(response => response.json())
}

/**
 * @name startDevProject
 * @desc user starts new project
 * @param fd - form data
 */
function startDevProject(fd) {
	return fetch(`${API_URL}/devproject/start/`, {
			method: 'POST',
			credentials: 'include',
			body: fd,
		})
		.then(response => response.json())
}

/**
 * @name uploadDevProject
 * @desc upload .zip file for dev project review
 * @param fd - form data
 */
function uploadDevProject(fd) {
	return fetch(`${API_URL}/devproject/upload_file`, {
			method: 'POST',
			credentials: 'include',
			body: fd,
		})
		.then(response => response.json())
}

/**
 * @name submitDevProject
 * @desc submit dev project for review
 * @param fd - form data
 */
function submitDevProject(fd) {
	return fetch(`${API_URL}/devproject/submit/`, {
			method: 'POST',
			credentials: 'include',
			body: fd,
		})
		.then(response => response.json())
}

export {
	checkSession,
	logOut,
	setUserPassword,
	setUserProfile,
	uploadAvatar,
	uploadCV,
	getProjects,
	getDevProjects,
	deleteFile,
	startDevProject,
	uploadDevProject,
	submitDevProject,
}
