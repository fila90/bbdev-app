/**
 * @name handleLanImg
 * @desc set correct language image
 * @param lng - string
 */
function handleLanImg(lng) {
  switch (lng) {
    case 'js':
    case 'vanilla js':
    case 'vanilla':
      return require('@/assets/img/js-logo.png');
      break;
    case 'mongo':
    case 'mongo db':
    case 'mongodb':
      return require('@/assets/img/mongodb-logo.png');
      break;
    case 'node':
    case 'nodejs':
    case 'node js':
      return require('@/assets/img/nodejs-logo.png');
      break;
    case 'react':
    case 'react native':
      return require('@/assets/img/react-logo.png');
      break;
    case 'css':
    case 'css 3':
      return require('@/assets/img/css-logo.png');
      break;
    case 'html':
    case 'html 5':
      return require('@/assets/img/html-logo.png');
      break;
    case 'vue':
    case 'vue.js':
      return require('@/assets/img/vue-logo.png');
      break;
    default:
      return require('@/assets/img/js-logo.png');
      break
  }
}

export {
  handleLanImg
}
