import Vue from 'vue'
import Router from 'vue-router'

import Layout from '@/views/Layout'
import Loading from '@/views/Loading'
import Dashboard from '@/views/Dashboard'

import editRoutes from '@/modules/edit/router'
import projectRoutes from '@/modules/project/router'
import devProjectRoutes from '@/modules/devproject/router'

const redirect = {
	path: '*',
	redirect: '/app'
}
const loading = {
	path: '/',
	name: 'loading',
	component: Loading
}
const appRoutes = {
	path: '/app',
	component: Layout,
	children: [{
			path: '',
			name: 'Dashboard',
			component: Dashboard,
			children: [...editRoutes],
		},
		...projectRoutes,
		...devProjectRoutes
	]
}

Vue.use(Router)

export default new Router({
	routes: [
		redirect,
		loading,
		appRoutes,
	]
})
