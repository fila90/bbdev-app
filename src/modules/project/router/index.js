import Project from '../views/Project'
import Description from '../views/Description'
import Resources from '../views/Resources'

export default [{
	path: 'project/:id',
	title: 'project',
	component: Project,
	redirect: {
		name: 'description'
	},
	children: [{
		path: 'description',
		name: 'description',
		component: Description
	}, {
		path: 'resources',
		name: 'resources',
		component: Resources
	}]
}]
