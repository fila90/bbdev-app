import Helper from '@/views/Helper'
import Profile from '../views/Profile'
import Password from '../views/Password'

export default [{
	path: 'edit',
	name: 'edit',
	component: Helper,
	children: [{
		path: 'profile',
		title: 'profile',
		name: 'profile',
		component: Profile
	}, {
		path: 'password',
		title: 'password',
		name: 'password',
		component: Password
	}]
}]
