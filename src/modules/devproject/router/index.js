import DevProject from '../views/DevProject'
import Review from '../views/Review'
import Files from '../views/Files'

export default [{
	path: 'dev-project/:id',
	title: 'dev-projec',
	component: DevProject,
	redirect: {
		name: 'review'
	},
	children: [{
		path: 'review',
		name: 'review',
		component: Review
	}, {
		path: 'files',
		name: 'files',
		component: Files
	}]
}]
