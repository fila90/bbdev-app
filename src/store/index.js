const user = localStorage.getItem('user')
	? JSON.parse(localStorage.getItem('user'))
	: {};
const projects = [];

export {
	user,
	projects
}
